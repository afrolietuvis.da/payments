package payments;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "myapp")
public class MyAppProperties {

    private String serverPort;

    public MyAppProperties() {}
}
