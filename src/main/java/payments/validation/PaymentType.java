package payments.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = PaymentTypeConstraintValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PaymentType {


    String message() default "Invalid payment type. TYPE1, TYPE2 and TYPE3 are supported";

    String[] value();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
