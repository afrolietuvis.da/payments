package payments.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class CurrencyConstraintValidator implements ConstraintValidator<Currency, String> {


    private List<String> supportedCurrencies;

    public void initialize(Currency inputCodes) {
        supportedCurrencies = Arrays.asList(inputCodes.value());
    }

    public boolean isValid(String theCode, ConstraintValidatorContext context) {
        return supportedCurrencies.contains(theCode.toUpperCase());
    }
}
