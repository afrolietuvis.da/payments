package payments.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class PaymentTypeConstraintValidator implements ConstraintValidator<PaymentType, String> {

    private List<String> types;

    public void initialize(PaymentType inputTypes) {
        types = Arrays.asList(inputTypes.value());
    }

    public boolean isValid(String theType, ConstraintValidatorContext context) {
        return types.contains(theType.toUpperCase());
    }
}
