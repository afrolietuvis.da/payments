package payments.DTO;


import lombok.Data;
import lombok.NoArgsConstructor;
import payments.validation.Currency;

import javax.persistence.Enumerated;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class PaymentDto {

    @payments.validation.PaymentType({"TYPE1","TYPE2","TYPE3"})
    private String paymentType;

    @Positive
    @NotNull
    @Digits(integer = 5, fraction = 2)
    private BigDecimal amount;

    @Currency({"EUR","USD"})
    private String currency;

    @NotBlank
    @NotNull
    private String creditorIban;

    @NotBlank
    @NotNull
    private String debtorIban;

    private String details;


    private String creditorBIC;

}
