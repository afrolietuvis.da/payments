package payments.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import payments.Currency;
import payments.DTO.PaymentDto;
import payments.DTO.PaymentStatus;
import payments.DTO.PaymentType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "payment")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

    private String debtorIban;

    private String creditorIban;

    private String creditorBIC;

    private BigDecimal feeOnCancel;

    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;

    private String details;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyy-MM-dd hh:mm")
    private LocalDateTime createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyy-MM-dd hh:mm")
    private LocalDateTime canceledOn;


    public static Payment of(PaymentDto paymentDto) {
        Payment payment = new Payment();
        payment.setPaymentType(PaymentType.valueOf(paymentDto.getPaymentType().toUpperCase()));
        payment.setAmount(paymentDto.getAmount());
        payment.setCreditorIban(paymentDto.getCreditorIban());
        payment.setDebtorIban(paymentDto.getDebtorIban());
        payment.setCurrency(Currency.valueOf(paymentDto.getCurrency().toUpperCase()));
        payment.setCreditorBIC(paymentDto.getCreditorBIC());
        payment.setDetails(paymentDto.getDetails());
        payment.setPaymentStatus(PaymentStatus.PENDING);
        payment.setCreatedOn(LocalDateTime.now());
        return payment;
    }
}
