package payments.model;

import lombok.Data;
import org.springframework.http.HttpStatus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Data
@Entity
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private HttpStatus responseStatus;
    private String message;

    public static Log of(String message,HttpStatus responseStatus){
        Log log = new Log();
        log.setMessage(message);
        log.setResponseStatus(responseStatus);
        return log;
    }

}
