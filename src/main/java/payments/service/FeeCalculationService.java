package payments.service;

import org.springframework.stereotype.Service;
import payments.model.Payment;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
public class FeeCalculationService {


    void calculateFee(Payment payment) {
        int hoursPassed = LocalDateTime.now().getHour() - payment.getCreatedOn().getHour();
        BigDecimal fee = payment.getPaymentType().getFeeOnCancel()
                .multiply(BigDecimal.valueOf(hoursPassed)
                        .multiply(payment.getAmount()));

        if (hoursPassed != 0) {
            payment.setFeeOnCancel(fee);
        } else {
            payment.setFeeOnCancel(BigDecimal.valueOf(0.00));
        }
    }


}
