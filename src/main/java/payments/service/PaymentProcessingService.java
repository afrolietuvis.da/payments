package payments.service;


import com.sun.jdi.InvalidTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import payments.DTO.PaymentStatus;
import payments.model.Payment;

import java.time.LocalDateTime;
import java.util.concurrent.CancellationException;

@Service
public class PaymentProcessingService {


    private final FeeCalculationService feeCalculator;

    public PaymentProcessingService(FeeCalculationService feeCalculator){
        this.feeCalculator = feeCalculator;
    }


    public Payment tryAndCancel(Payment payment) throws CancellationException {
        if (canCancel(payment)) {
            cancelPayment(payment);
            feeCalculator.calculateFee(payment);
            return payment;
        } else throw new CancellationException();
    }

    private boolean canCancel(Payment payment) {
        return payment.getCreatedOn().getDayOfYear() == LocalDateTime.now().getDayOfYear();
    }

    private void cancelPayment(Payment payment) {
        payment.setPaymentStatus(PaymentStatus.CANCELED);
        payment.setCanceledOn(LocalDateTime.now());
    }
}
