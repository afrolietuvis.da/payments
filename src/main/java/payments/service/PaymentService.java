package payments.service;

import com.sun.jdi.InvalidTypeException;
import javassist.NotFoundException;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import payments.DTO.PaymentDto;
import payments.DTO.PaymentStatus;
import payments.model.Payment;
import payments.repository.PaymentRepository;
import payments.utils.exception.CustomException;
import payments.utils.validator.PaymentValidatorFactory;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Log
@Service
public class PaymentService {

    private final PaymentRepository repository;
    private final PaymentProcessingService paymentProcessor;
    private final PaymentValidatorFactory paymentValidatorFactory;

    public PaymentService(PaymentRepository repository, PaymentProcessingService paymentProcessor, PaymentValidatorFactory paymentValidatorFactory) {
        this.repository = repository;
        this.paymentProcessor = paymentProcessor;
        this.paymentValidatorFactory = paymentValidatorFactory;
    }

    public Optional<Payment> save(PaymentDto paymentDto) throws CustomException {
        Payment payment = Payment.of(paymentDto);
        paymentValidatorFactory.build(payment.getPaymentType()).validate(payment);
        log.info("Payment successfully saved");
        return Optional.of(repository.save(payment));

    }

    public List<Payment> findAllFilteredAndSorted(Optional<String> status, boolean sorted) {
        return sorted ?
                status.map(s -> repository.findByPaymentStatusOrderByAmountDesc(statusFromString(s))).
                        orElse(repository.findAll(Sort.by(Sort.Direction.ASC, "Amount")))
                :
                status.map(s -> repository.findByPaymentStatus(statusFromString(s))).
                        orElse(repository.findAll());
    }


    @Transactional
    public void cancelPaymentById(long id) throws NotFoundException {
        this.repository.findById(id).
                map(paymentProcessor::tryAndCancel).
                orElseThrow(() -> new NotFoundException("Payment with id " + id + " not found"));
    }

    PaymentStatus statusFromString(String string) {
        return PaymentStatus.valueOf(string.toUpperCase());
    }


    public Payment findOne(Long id) throws NotFoundException {
        return repository.findById(id).orElseThrow(()-> new NotFoundException("Payment with id " + id + " not found"));
    }
}
