package payments.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import payments.model.Log;


@Repository
public interface LogRepository extends JpaRepository<Log,Long> {
}
