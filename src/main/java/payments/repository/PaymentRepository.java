package payments.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import payments.DTO.PaymentStatus;
import payments.model.Payment;

import java.util.List;
import java.util.Optional;


@Repository
public interface PaymentRepository extends JpaRepository<Payment,Long> {


    List<Payment> findByPaymentStatusOrderByAmountDesc(PaymentStatus status);

    List<Payment> findByPaymentStatus(PaymentStatus status);


}
