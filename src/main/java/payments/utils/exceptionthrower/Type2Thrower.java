package payments.utils.exceptionthrower;


import payments.utils.exception.CustomException;
import payments.utils.exception.InvalidInputForType2PaymentException;

public class Type2Thrower implements ExceptionThrower {
    @Override
    public void throwException(String message) throws CustomException {
        throw new InvalidInputForType2PaymentException(message);
    }
}
