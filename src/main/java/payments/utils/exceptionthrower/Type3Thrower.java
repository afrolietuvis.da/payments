package payments.utils.exceptionthrower;

import payments.utils.exception.CustomException;
import payments.utils.exception.InvalidInputForType3PaymentException;

public class Type3Thrower implements ExceptionThrower {
    @Override
    public void throwException(String message) throws CustomException {
        throw new InvalidInputForType3PaymentException(message);
    }
}
