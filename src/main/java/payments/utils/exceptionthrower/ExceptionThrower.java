package payments.utils.exceptionthrower;


import payments.utils.exception.CustomException;

public interface ExceptionThrower {


    void throwException(String message) throws CustomException;

}
