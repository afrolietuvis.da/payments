package payments.utils.exceptionthrower;


import org.springframework.stereotype.Service;
import payments.DTO.PaymentType;

@Service
public class ExceptionThrowerFactory {

    public ExceptionThrower build(PaymentType paymentType){
       if(paymentType.toString().equalsIgnoreCase(PaymentType.TYPE1.toString())){
           return new Type1Thrower();
       }
       if(paymentType.toString().equalsIgnoreCase(PaymentType.TYPE2.toString())){
           return new Type2Thrower();
       }
       if (paymentType.toString().equalsIgnoreCase(PaymentType.TYPE3.toString())){
           return new Type3Thrower();
       }
       throw new UnsupportedOperationException("Not supported yet.");
    }

}
