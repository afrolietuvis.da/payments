package payments.utils.exceptionthrower;


import payments.utils.exception.CustomException;
import payments.utils.exception.InvalidInputForType1PaymentException;

public class Type1Thrower implements ExceptionThrower {
    @Override
    public void throwException(String message) throws CustomException {
        throw new InvalidInputForType1PaymentException(message);
    }
}
