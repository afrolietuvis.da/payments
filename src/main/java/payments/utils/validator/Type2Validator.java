package payments.utils.validator;


import payments.DTO.PaymentDto;
import payments.model.Payment;
import payments.utils.exception.CustomException;
import payments.utils.exceptionthrower.ExceptionThrower;
import payments.utils.exceptionthrower.ExceptionThrowerFactory;

public class Type2Validator extends PaymentValidator {
    @Override
    public void validate(Payment payment) throws CustomException {
        ExceptionThrower et = new ExceptionThrowerFactory().build(payment.getPaymentType());
        StringBuilder errorBuilder = new StringBuilder();
        if (!isUSD(payment.getCurrency()))
            errorBuilder.append(payment.getCurrency()).append(" currency is not supported for ").append(payment.getPaymentType()).append(" payments. ");
        if(!errorBuilder.toString().isBlank())
        et.throwException(errorBuilder.toString());
    }
}
