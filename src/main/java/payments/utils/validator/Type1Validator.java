package payments.utils.validator;


import payments.DTO.PaymentDto;
import payments.model.Payment;
import payments.utils.exception.CustomException;
import payments.utils.exceptionthrower.ExceptionThrower;
import payments.utils.exceptionthrower.ExceptionThrowerFactory;

public class Type1Validator extends PaymentValidator{

    @Override
    public void validate(Payment payment) throws CustomException {
        ExceptionThrower et = new ExceptionThrowerFactory().build(payment.getPaymentType());
        StringBuilder errorBuilder = new StringBuilder();
        if(!isEuro(payment.getCurrency()))
            errorBuilder.append(payment.getCurrency()).append(" currency is not supported for ").append(payment.getPaymentType()).append(" payments. ");
        if(payment.getDetails() == null || payment.getDetails().isBlank())
            errorBuilder.append("Details field is required for ").append(payment.getPaymentType()).append(" payments.");
        if(!errorBuilder.toString().isBlank())
            et.throwException(errorBuilder.toString());
    }
}
