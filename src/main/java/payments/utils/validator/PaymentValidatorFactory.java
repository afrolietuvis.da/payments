package payments.utils.validator;

import org.springframework.stereotype.Service;
import payments.DTO.PaymentType;

@Service
public class PaymentValidatorFactory {

    public PaymentValidator build(PaymentType paymentType){
        if(paymentType.equals(PaymentType.TYPE1)){
            return new Type1Validator();
        }
        if(paymentType.equals(PaymentType.TYPE2)){
            return new Type2Validator();
        }
        if (paymentType.equals(PaymentType.TYPE3)){
            return new Type3Validator();
        }
        throw new UnsupportedOperationException("Payment type not supported.");
    }
}
