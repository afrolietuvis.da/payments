package payments.utils.validator;



import payments.Currency;
import payments.DTO.PaymentDto;
import payments.model.Payment;
import payments.utils.exception.CustomException;

import java.util.List;


public abstract class PaymentValidator {

    private final List<Currency> supportedCurrencies = List.of(Currency.EUR,Currency.USD);

    boolean isEuro(Currency currency){
        return currency.equals(Currency.EUR);
    }
    boolean isUSD(Currency currency){
        return currency.equals(Currency.USD);
    }
    boolean isSupportedCurrency(Currency currency){
        return supportedCurrencies.contains(currency);
    }

   public abstract void validate(Payment payment) throws CustomException;
}
