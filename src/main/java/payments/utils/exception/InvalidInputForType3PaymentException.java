package payments.utils.exception;

public class InvalidInputForType3PaymentException extends CustomException{
    public InvalidInputForType3PaymentException(String message) {
        super(message);
    }
}
