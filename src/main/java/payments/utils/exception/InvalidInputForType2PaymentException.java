package payments.utils.exception;

public class InvalidInputForType2PaymentException extends CustomException{
    public InvalidInputForType2PaymentException(String message) {
        super(message);
    }
}
