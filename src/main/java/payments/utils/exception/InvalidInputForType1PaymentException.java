package payments.utils.exception;


public class InvalidInputForType1PaymentException extends CustomException {
    public InvalidInputForType1PaymentException(String message) {
        super(message);
    }
}
