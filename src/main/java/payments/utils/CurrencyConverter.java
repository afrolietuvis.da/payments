package payments.utils;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public final class CurrencyConverter {
    private final BigDecimal eurToUsd = new BigDecimal("1.21");
    private final BigDecimal usdToEur = new BigDecimal("0.82");

    BigDecimal convertEurToUsd(BigDecimal amount){
        return amount.multiply(eurToUsd);
    }
    BigDecimal convertUsdToEur(BigDecimal amount){
        return amount.multiply(usdToEur);
    }
}
