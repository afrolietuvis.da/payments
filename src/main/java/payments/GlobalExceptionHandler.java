package payments;


import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import payments.model.Log;
import payments.repository.LogRepository;
import payments.utils.error.ErrorMessage;
import payments.utils.error.FieldErrorMessage;
import payments.utils.exception.CustomException;

import javax.xml.bind.ValidationException;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.stream.Collectors;


@lombok.extern.java.Log
@RestController
@ControllerAdvice

public class GlobalExceptionHandler {


    private final List<String> loggedExceptions =  List.of(
            "InvalidInputForType1PaymentException",
            "InvalidInputForType2PaymentException");

    private final LogRepository logRepository;

    public GlobalExceptionHandler(LogRepository logRepository){
        this.logRepository = logRepository;
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    ErrorMessage exceptionHandler(ValidationException e) {
        return new ErrorMessage(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    List<FieldErrorMessage> exceptionHandler(MethodArgumentNotValidException e) {
        log.warning(e.getMessage());
        return e.getBindingResult().getFieldErrors()
                .stream()
                .map(fieldError -> new FieldErrorMessage(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    ErrorMessage exceptionHandler(CancellationException e) {
        return new ErrorMessage(HttpStatus.BAD_REQUEST, "Can only cancel on the same day before 00:00");
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    ErrorMessage exceptionHandler(NotFoundException e) {
        return new ErrorMessage(HttpStatus.NOT_FOUND, e.getMessage());
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    ErrorMessage exceptionHandler(CustomException e) {
        log.warning(e.getMessage());
        if(loggedExceptions.contains(e.getClass().getSimpleName()))
            logRepository.save(Log.of(e.getMessage(), HttpStatus.BAD_REQUEST));
        return new ErrorMessage(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    ErrorMessage exceptionHandler(InvalidFormatException e){
        String message = e.getMessage().contains("paymentType") ? "Invalid payment type entered"
                : e.getMessage().contains("Currency") ? "This currency is not supported yet."
                : e.getMessage();
        return new ErrorMessage(HttpStatus.BAD_REQUEST,message);
    }


}
