package payments.controller;


import javassist.NotFoundException;
import org.apache.coyote.Response;
import org.springframework.core.serializer.support.SerializationFailedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import payments.DTO.PaymentDto;
import payments.model.Payment;
import payments.service.PaymentService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/payments")
public class PaymentController {


   private final PaymentService paymentService;

   public PaymentController(PaymentService paymentService){
       this.paymentService = paymentService;
   }


    @PostMapping
    Payment create(@Valid @RequestBody PaymentDto paymentDto) throws Exception {
        return paymentService.save(paymentDto).orElseThrow( () -> new SerializationFailedException("Payment was not saved"));

    }
    @GetMapping("/{id}")
    ResponseEntity<Payment> getPayment(@PathVariable Long id) throws NotFoundException {
        return ResponseEntity.ok(paymentService.findOne(id));
    }

    @GetMapping
    ResponseEntity<List<Payment>> read(@RequestParam(value = "status", required = false) Optional<String> status,
                       @RequestParam(value = "sorted", required = false) boolean sorted) {
        return ResponseEntity.ok(paymentService.findAllFilteredAndSorted(status, sorted));
    }


    @PutMapping("/cancel")
    void cancel(@RequestParam(value = "id") long id) throws NotFoundException {
       paymentService.cancelPaymentById(id);
    }

}
