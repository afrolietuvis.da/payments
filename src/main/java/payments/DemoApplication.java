package payments;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@Log
@SpringBootApplication
public class DemoApplication {

    private final MyAppProperties props;

    public DemoApplication(MyAppProperties props){
        this.props = props;
    }

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DemoApplication.class);
        application.setAdditionalProfiles("dev");
        SpringApplication.run(DemoApplication.class, args);
        log.warning("Application started");
    }

    @Value("${myapp.server-port}")
    String serverPort;

    @Bean
    @Profile("!test")
    CommandLineRunner values(){
        return args -> {
            log.info("Server running on port: " + props.getServerPort());
        };
    }

}
