package payments;

import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import payments.DTO.PaymentStatus;
import payments.model.Payment;
import payments.repository.PaymentRepository;

import java.math.BigDecimal;
import java.util.List;

@DataJpaTest
@ActiveProfiles("test")
public class DerivedQueryTests {

    @Autowired
    PaymentRepository paymentRepository;

    @MockBean
    MyAppProperties props;


    @BeforeEach
    public void setUp(){
        paymentRepository.deleteAll();
    }

    @Test
    public void shouldFindCanceledPayments(){
        final Payment payment1 =  createPayment(PaymentStatus.CANCELED);
        final Payment payment2 =  createPayment(PaymentStatus.CANCELED);
        final Payment payment3 =  createPayment(PaymentStatus.PENDING);

        paymentRepository.save(payment1);
        paymentRepository.save(payment2);
        paymentRepository.save(payment3);

        List<Payment> canceledPayments = paymentRepository.findByPaymentStatus(PaymentStatus.CANCELED);
        assertEquals(canceledPayments.size(), 2);
        assertEquals(canceledPayments.get(0), payment1);
        assertEquals(canceledPayments.get(1), payment2);
    }


    private Payment createPayment(PaymentStatus status){
        final Payment payment = new Payment();
        payment.setPaymentStatus(status);
        payment.setCurrency(Currency.EUR);
        payment.setAmount(BigDecimal.valueOf(100));
        return payment;
    }
}
