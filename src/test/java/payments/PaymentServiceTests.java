package payments;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import payments.DTO.PaymentDto;
import payments.DTO.PaymentType;
import payments.model.Payment;
import payments.repository.PaymentRepository;
import payments.service.PaymentProcessingService;
import payments.service.PaymentService;
import payments.utils.exception.CustomException;
import payments.utils.validator.PaymentValidatorFactory;

import java.math.BigDecimal;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
public class PaymentServiceTests {
//
//    @InjectMocks
//    PaymentService service;
//    @Autowired
//    PaymentRepository repository;
//
//    @MockBean
//    MyAppProperties props;
//
//    @MockBean
//    PaymentValidatorFactory factory;
//    @MockBean
//    PaymentProcessingService processor;
//
//    @BeforeEach
//    public void initPaymentService(){
//        service = new PaymentService(repository,processor,factory);
//    }
//
//    @Test
//    public void should_save_payment_with_valid_data() throws CustomException {
//        PaymentDto paymentDto = new PaymentDto();
//        paymentDto.setCurrency("EUR");
//        paymentDto.setPaymentType(PaymentType.TYPE1);
//        paymentDto.setAmount(BigDecimal.valueOf(100));
//        paymentDto.setDebtorIban("some iban");
//        paymentDto.setCreditorIban("some iban");
//
//        Payment created = service.save(paymentDto);
//        assertEquals(created.getCurrency(), Currency.EUR);
//
//    }


}
