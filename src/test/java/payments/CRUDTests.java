package payments;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import payments.DTO.PaymentType;
import payments.model.Payment;
import payments.repository.PaymentRepository;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
class CRUDTests {


   @Autowired
   PaymentRepository paymentService;


   @Test
    public void shouldPerformCRUDOperations(){



       final Payment payment = new Payment();
       payment.setPaymentType(PaymentType.TYPE1);
       payment.setAmount(BigDecimal.valueOf(100));
       payment.setCurrency(Currency.EUR);

       paymentService.save(payment);

       assertThat(paymentService.findAll())
               .hasSize(1)
               .first()
               .isEqualTo(payment);

       paymentService.deleteById(payment.getId());

       assertThat(paymentService.count()).isZero();

    }

}
